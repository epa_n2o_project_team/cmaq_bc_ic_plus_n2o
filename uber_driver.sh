#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### This script clones and runs code either from
### * the `git` repository @ https://bitbucket.org/epa_n2o_project_team/cmaq_bc_ic_plus_n2o
###   in which case, you probably wanna check its README before running this.

### * the current folder (for code you haven't committed).

### If you decide to run this, you should first

### * un/comment var=ACCESS to reflect your available/desired file-transfer protocol
### * if using ACCESS='file', edit `FILE_LIST`s below
### * edit var=WORKSPACE_ROOT to state where to create your repo clone
### * remember to enable netCDF-4 on your system, e.g.
### > module add netcdf-4.1.2 # on terrae (a cluster on which I work)

### and run this from dir/folder containing this code like (e.g.)
### START="$(date)" ; echo -e "START=${START}" ; rm -fr /tmp/cmaq_bc_ic_plus_n2o/ ; ./uber_driver.sh ; END="$(date)" ; echo -e "  END=${END}" ; echo -e "START=${START}"

### TODO: make this run outside ./! e.g.:
### START="$(date)" ; echo -e "START=${START}" ; rm -fr /tmp/cmaq_bc_ic_plus_n2o/ ; ~/code/regridding/CMAQ_BC_IC_plus_N2O/uber_driver.sh ; END="$(date)" ; echo -e "  END=${END}" ; echo -e "START=${START}"

### Note: this script requires
### * `date`
### * `bash` version recent enough for string-replacement syntax
### * `true` and `false` executables (e.g., /bin/true , /bin/false) for "real bash booleans"

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

# TODO: take switches for help, debugging, no/eval, target drive
THIS="$0"
THIS_FN="$(basename ${THIS})"
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### Strings for needed resources (see setup_resources)

## These are set here (only):
WORKSPACE_ROOT='/tmp' # or location of your choice: repo/workspace created in subdir
DRIVER_FN='BC_IC_N2O_driver.sh' # what actually does the work. TODO: get from commandline

## Many more strings set in resource_strings
## Note: will also be run in ${DRIVER_FN}, so edit ./resource_strings.sh to, e.g., not/use output
#source "./resource_strings.sh"
source "${THIS_DIR}/resource_strings.sh"

## Local settings which also require resource_strings
WORKSPACE_DIR="${WORKSPACE_ROOT}/${REMOTE_PROJECT_NAME}" # from resource_strings

### Activity-control values

## Following are "real bash booleans": alternate values are (executables, not strings, so not quoted) true , false
## Does our platform allow git/ssh? (EPA clusters, e.g., EMVL and HPCC do not)
CAN_GIT_SSH=false
## Does our platform have required HTTP certificates? If not, git must not check for certificates.
HAVE_HTTP_CERTS=false
if ${HAVE_HTTP_CERTS} ; then
  export GIT_CLONE_PREFIX=''
else
  export GIT_CLONE_PREFIX='GIT_SSL_NO_VERIFY=true' # set for ${DRIVER_FN}
fi

## Modify driver call
DRIVER="${GIT_CLONE_PREFIX} ./${DRIVER_FN}"

## modes of access to sources (see setup_sources): either copy files or get from repo
# ACCESS='file'       # copy code in current directory, uses no repository
# ACCESS='git/ssh'    # outgoing ssh works (preferred if available), uses REMOTE_PROJECT_GIT_URI from resource_strings.sh
# ACCESS='http+cert'  # outgoing ssh blocked, but you have certificates
ACCESS='http-cert'  # (terrae) outgoing ssh blocked (e.g., no certs installed) also uses REMOTE_PROJECT_HTTP_URI from resource_strings.sh

# if [[ "${ACCESS}" == 'file' ]], you'll also need
REQUIRED_FILE_LIST="./${DRIVER_FN} \
  BC_IC_helpers.ncl \
  conditions_plus_N2O.ncl \
  resource_strings.sh \
  verticalize_conditions.ncl \
  write_N2O.ncl "

# Following should be gotten from regrid_utils, but if you need to modify them, you need to add them to REQUIRED_FILE_LIST:
#   attributes.ncl \
#   bash_utilities.sh \
#   compare.ncl \
#   dimensions.ncl \
#   get_filepath_from_template.ncl \
#   IOAPI.ncl \
#   string.ncl \
#   time.ncl \

# These control files will be created if not found.
OPTIONAL_FILE_LIST="BCs_to_process.txt \
  ICs_to_process.txt "

# TODO: get file lists from ${DRIVER_FN}

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

function setup_sources {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local LOOP_MESSAGE_PREFIX
  local LOOP_ERROR_PREFIX

  if [[ "${ACCESS}" == 'file' ]] ; then
    if [[ ! -x "./${DRIVER_FN}" ]] ; then
      echo -e "${ERROR_PREFIX} driver=='${DRIVER_FN}' not executable (with ACCESS==${ACCESS}), exiting"
      exit 2
    else

      ## These are required, so fail on error.
      for CMD in \
        "mkdir -p ${WORKSPACE_DIR}" \
        "cp ${REQUIRED_FILE_LIST} ${WORKSPACE_DIR}/ " \
      ; do
        LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
        LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
        echo -e "${LOOP_MESSAGE_PREFIX}"
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
          echo -e "${LOOP_ERROR_PREFIX} failed (with ACCESS==${ACCESS}), exiting"
          exit 3
        fi
      done

      ## These are optional, so !fail on error.
      for CMD in \
        "cp ${OPTIONAL_FILE_LIST} ${WORKSPACE_DIR}/ " \
      ; do
        LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
        LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
        echo -e "${LOOP_MESSAGE_PREFIX}"
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
          echo -e "${LOOP_ERROR_PREFIX} failed (with ACCESS==${ACCESS}), ignored"
        fi
      done

    fi # [[ ! -x "./${DRIVER_FN}" ]]

  else # ACCESS != 'file', i.e., from repo

    if   [[ "${ACCESS}" == 'git/ssh' ]] ; then
      if [[ -n "${CAN_GIT_SSH}" && "${CAN_GIT_SSH}" == 'TRUE' ]] ; then
        REPO_URI="${REMOTE_PROJECT_GIT_URI}"
      else
        echo -e "${ERROR_PREFIX} cannot use access==git/ssh on this platform, exiting"
        exit 4
      fi
    elif [[ "${ACCESS}" == 'http+cert' ]] ; then
      if   [[ -z "${REMOTE_PROJECT_HTTP_URI}" ]] ; then
        echo -e "${ERROR_PREFIX} REMOTE_PROJECT_HTTP_URI undefined, thus cannot use access mode='${ACCESS}', exiting"
        exit 5
      elif [[ -n "${HAVE_HTTP_CERTS}" && "${HAVE_HTTP_CERTS}" == 'TRUE' ]] ; then
        REPO_URI="${REMOTE_PROJECT_HTTP_URI}"
      else
        echo -e "${ERROR_PREFIX} cannot use access==http+cert on this platform, exiting"
        exit 6
      fi
    elif [[ "${ACCESS}" == 'http-cert' ]] ; then
      if [[ -z "${REMOTE_PROJECT_HTTP_URI}" ]] ; then
        echo -e "${ERROR_PREFIX} REMOTE_PROJECT_HTTP_URI undefined, thus cannot use access mode='${ACCESS}', exiting"
        exit 7
      else 
        REPO_URI="${REMOTE_PROJECT_HTTP_URI}"
      fi
    fi

    for CMD in \
      "pushd ${WORKSPACE_ROOT}" \
      "${GIT_CLONE_PREFIX} git clone ${REPO_URI}" \
      "popd" \
    ; do
      LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
      LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
      echo -e "${LOOP_MESSAGE_PREFIX}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${LOOP_ERROR_PREFIX} failed (with ACCESS==${ACCESS}), exiting"
        exit 8
      fi
    done

  fi # end testing ACCESS
} # end function setup_sources

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

if [[ -z "${WORKSPACE_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} WORKSPACE_DIR not defined, exiting"
  exit 9
fi

if [[ -d "${WORKSPACE_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} workspace dir/folder='${WORKSPACE_DIR}' exists: move or delete it before running this script. Exiting"
  exit 10
fi

if [[ -z "${ACCESS}" ]] ; then
  echo -e "${ERROR_PREFIX} ACCESS not defined: did you uncomment one of the choices above? exiting"
  exit 11
fi

#   'setup_sources' \           # in this file
#   'setup_resources' \         # in resource_strings
#   "pushd ${WORKSPACE_DIR}" \  # where the code to run is, using ...
#   "${DRIVER}" \               # ... its driver
for CMD in \
  'setup_sources' \
  'setup_resources' \
  "pushd ${WORKSPACE_DIR}" \
  "${DRIVER}" \
; do
#   "ls -alhR ${WORKSPACE_DIR}" \
  LOOP_MESSAGE_PREFIX="${THIS_FN}::main loop::${CMD}:"
  LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
  echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
  eval "${CMD}" # comment this out for NOPing, e.g., to `source`
  if [[ $? -ne 0 ]] ; then
    echo -e "\n${LOOP_ERROR_PREFIX} failed or not found\n"
    exit 12
  fi
done
exit 0
# end uber_driver.sh
