*(part of the [AQMEII-NA_N2O][AQMEII-NA_N2O wiki home] family of projects)*

**table of contents**

[TOC]

# open-source notice

Copyright 2013, 2014 Tom Roche <Tom_Roche@pobox.com>

This project's content is free software: you can redistribute it and/or modify it provided that you do so as follows:

* under the terms of the [GNU Affero General Public License][GNU Affero General Public License HTML @ GNU] as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

This project's content is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU Affero General Public License][GNU Affero General Public License local text] for more details.

![distributed under the GNU Affero General Public License](../../downloads/Affero_badge__agplv3-155x51.png)

[GNU Affero General Public License local text]: ./COPYING
[GNU Affero General Public License HTML @ GNU]: https://www.gnu.org/licenses/agpl.html

# summary

Suppose you have [CMAQ][CMAQ main page @ CMAS]-ready [boundary (BC)][CMAQ BC file metadata] and [initial (IC)][CMAQ IC file metadata] condition [IOAPI][]/[netCDF][netCDF @ wikipedia] files (collectively known as *conditions* files) containing a number of species (typically those required for air-quality (AQ) applications), but not some particular species of interest. This project takes as input

* a set of CMAQ AQ BCs and ICs over {[AQMEII-NA][AQMEII-NA spatial domain], 2008} used with the [EPA CDC/PHASE-2008][] study
* a datasource for N2O background concentrations (currently from [NOAA ESRL HATS][NOAA ESRL HATS homepage])

and outputs a set of well-formed CMAQ BCs or ICs (corresponding one-to-one to the input) containing all of the AQ species, plus N2O.

[AQMEII-NA_N2O wiki home]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Home
[AQMEII-NA spatial domain]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/AQMEII-NA_spatial_domain
[CMAQ BC file metadata]: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD#BNDY_CONC_1:_Boundary_conditions
[CMAQ IC file metadata]: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD#INIT_CONC_1:_Initial_conditions
[CMAQ main page @ CMAS]: http://www.cmaq-model.org/
[EPA CDC/PHASE-2008]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/EPA_CDC_PHASE_run_for_2008
[IOAPI]: http://www.baronams.com/products/ioapi/
[netCDF @ wikipedia]: http://en.wikipedia.org/wiki/NetCDF#Format_description
[NOAA ESRL HATS homepage]: http://www.esrl.noaa.gov/gmd/hats/

# description

Uses [bash][bash @ wikipedia] to drive [NCL][NCL @ wikipedia] code to

1. Read appropriate N2O atmospheric concentrations, in `ppmv` as required by [CMAQ][CMAQ main page @ CMAS] [BC][CMAQ BC file metadata] and [IC][CMAQ IC file metadata] files. Sources may include 
    * hard-coded input from an ungridded source of spatially- and temporally-coarse mean concentrations (e.g., [NOAA ESRL HATS N2O concentrations][])
    * *(planned)* spatially- and temporally-matching concentration grids (someday to derive from a [3D-regrid project][MOZART-global_to_AQMEII-NA])
1. Read {"well-formed," AQ-only, N2O-free, possibly vertically inconsistent} CMAQ BCs and ICs.
1. Write {well-formed, "reverticalized," AQ-only, N2O-free} CMAQ BCs and ICs, due to unexpected [vertical-layer-boundary mismatch][OAQPS vertical mismatch] between [received IC/BCs][OAQPS IC/BCs] and [meteorology][OAQPS meteorology]. (Confirming well-formedness of output WRT [IOAPI][] may require use of [VERDI][].)
1. Write {well-formed, vertically consistent, N2O-augmented} output files suitable for use in subsequent modeling.

[bash @ wikipedia]: http://en.wikipedia.org/wiki/Bash_%28Unix_shell%29
[MOZART-global_to_AQMEII-NA]: https://bitbucket.org/epa_n2o_project_team/mozart-global-to-aqmeii-na
[NCL @ wikipedia]: http://en.wikipedia.org/wiki/NCAR_Command_Language
[NOAA ESRL HATS N2O concentrations]: ftp://ftp.cmdl.noaa.gov/hats/n2o/combined/HATS_global_N2O.txt
[R @ wikipedia]: http://en.wikipedia.org/wiki/R_%28programming_language%29
[VERDI]: http://www.verdi-tool.org/
[OAQPS vertical mismatch]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/OAQPS_CDC_PHASE_2008#!vertical-mismatch
[OAQPS IC/BCs]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/OAQPS_CDC_PHASE_2008#!model-ready-input-paths
[OAQPS meteorology]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/OAQPS_CDC_PHASE_2008#!model-ready-input-paths

# operation

To run this code,

1. `git clone` this repo. (See commandlines on the [project homepage][CMAQ_BC_IC_plus_N2O project homepage], where you probably are now.)
1. `cd` to its working directory (where you cloned it to).
1. Setup your applications and paths.
    1. Download a copy of [these bash utilities][bash_utilities.sh] to the working directory.
    1. Open the file in in an editor! You will probably need to edit its functions `setup_paths` and `setup_apps` to make it work on your platform. Notably you will want to point it to your PDF viewer and NCL and R executables.
    1. Open the [resource strings][resource_strings.sh] (in this repo/directory/folder) in an editor. Make the paths point to your inputs, outputs, etc.
    1. You may also want to open the [driver (bash) script][BC_IC_N2O_driver.sh] in an editor and take a look. Having edited resources (in previous steps), the driver should run Out Of The Box, but you might need to tweak something. In the worst case, you could hardcode your paths and apps in the driver.
    1. By default, the [driver][BC_IC_N2O_driver.sh]
        1. will process all of the input conditions files in `BC_AQ_RAW_DIR` (for BCs) and `IC_AQ_RAW_DIR` (for ICs). If you *don't* want this default behavior, override it by creating files to specify the BCs or ICs which you *want* to process:
            1. Create a text file (at the path specified by `BCs_TO_PROCESS_FP`) containing the FQ paths to each BC you want to process (one path per line). Those BCs, and only those, will be processed, in the order specified.
            1. The procedure for ICs is analogous: e.g., the relevant path is specified by `ICs_TO_PROCESS_FP`.
        Note that the driver will create these text files if run in the default manner, so the user can edit them for subsequent runs if desired (or just delete them).
        1. will *not* process any raw input conditions files for which matching (by date) output files are found in the appropriate dir/folder for verticalized output (specified by `BC_AQ_VERT_DIR` or `IC_AQ_VERT_DIR`). Matching output is presumed to have been previously verticalized (though this is unfortunately not currently checked.)
    1. Once you've got this project working, you may want to fork it. If so, you can automate running your changes with [uber_driver.sh][] (changing that as needed, too).
1. Run [the driver][BC_IC_N2O_driver.sh]: `$ ./BC_IC_N2O_driver.sh` (Remember to prefix with `GIT_CLONE_PREFIX='GIT_SSL_NO_VERIFY=true'` if running on a platform without the necessary SSL certificates, such as `infinity` or `terrae`.) This will download inputs, then run ...
    1. [write_N2O.ncl][], which will write an N2O datasource file (currently [this][N2O_conc_NH_monthly.nc], used for both BCs and ICs) if not available.
    1. [verticalize_conditions.ncl][], which will loop over the set of paths to CMAQ conditions files passed to it by [the driver][BC_IC_N2O_driver.sh], changing their vertical layers to match the [input meteorology][OAQPS meteorology]
    1. [IC_plus_N2O.ncl][], which will loop over the set of paths to CMAQ IC files passed to it by [the driver][BC_IC_N2O_driver.sh],
        * *(planned)* verifying consistency of their vertical layers with sample meteorology
        * augmenting those files with species=`N2O`.
    1. [BC_plus_N2O.ncl][], which will perform similar functions WRT passed BC files.

[CMAQ_BC_IC_plus_N2O project homepage]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o_integration
[bash_utilities.sh]: https://bitbucket.org/epa_n2o_project_team/regrid_utils/src/HEAD/bash_utilities.sh?at=master
[resource_strings.sh]: https://bitbucket.org/epa_n2o_project_team/regrid_utils/src/HEAD/resource_strings.sh?at=master
[BC_IC_N2O_driver.sh]: ../../src/HEAD/BC_IC_N2O_driver.sh?at=master
[uber_driver.sh]: ../../src/HEAD/uber_driver.sh?at=master
[write_N2O.ncl]: ../../src/HEAD/write_N2O.ncl?at=master
[verticalize_conditions.ncl]: ../../src/HEAD/verticalize_conditions.ncl?at=master
[IC_plus_N2O.ncl]: ../../src/HEAD/IC_plus_N2O.ncl?at=master
[N2O_conc_NH_monthly.nc]: ../../downloads/N2O_conc_NH_monthly.nc
[BC_plus_N2O.ncl]: ../../src/HEAD/BC_plus_N2O.ncl?at=master

# TODOs

1. Move all these TODOs to [issue tracker][CMAQ_BC_IC_plus_N2O issues].
1. [`uber_driver.sh`][uber_driver.sh] or [`resource_strings.sh`][resource_strings.sh]: add provision to {copy, not create} [N2O datasource file][N2O_conc_NH_monthly.nc] from `uber_driver` if desired: it's small *now* ...
1. [`verticalize_conditions.ncl`][verticalize_conditions.ncl]: on inconsistency: pretty-print values side-by-side for easier comparison.
1. A meta-project *TODO* mostly for `regrid_utils`: create a home for generic filepath utilities currently in [`BC_IC_helpers.ncl`][BC_IC_helpers.ncl]:
    1. Create home for generic filepath utilities in `regrid_utils` with (e.g.) filename=`filepaths.ncl`.
    1. Make it `load` existing `regrid_utils/get_filepath_from_template.ncl`
    1. Move generic filepath utilities currently in `./BC_IC_helpers.ncl` and `regrid_utils/get_filepath_from_template.ncl` to `regrid_utils/filepaths.ncl`. Particularly, move `BC_IC_helpers.ncl::get_filepaths_from_file`, which has an exact copy @ [`IOAPI_spatiotemporality_checker/IOAPIST_helpers.ncl`][IOAPI_spatiotemporality_checker/IOAPIST_helpers.ncl].
1. Some meta-project *TODOs* (i.e., that belong in **every** project's *TODOs*):
    1. `*.sh`: use "real bash booleans" à la [`uber_driver.sh`][uber_driver.sh].
    1. `*.ncl`: put all body code (outside of `function`s) inside a `begin`/`end` block per [NCL recommendation](https://www.ncl.ucar.edu/Document/Manuals/Ref_Manual/NclStatements.shtml)
    1. `*.ncl`, `*.sh`:
        1. `*.ncl`: `exit()` -> `status_exit(n)` with `n` sequenced.
        1. `*.sh`: show {errorcodes, return values} to user! for use in debugging.
    1. `uber_driver.sh`: should be runnable from anywhere in filesystem, e.g.

        START="$(date)" ; echo -e "START=${START}" ; rm -fr <path to workspace/> ; <path to local repo/>/uber_driver.sh ; END="$(date)" ; echo -e "  END=${END}" ; echo -e "START=${START}"

       rather than, as current, requiring running from `./`, e.g.,

        pushd <path to local repo/> ; START="$(date)" ; echo -e "START=${START}" ; rm -fr <path to workspace/> ; ./uber_driver.sh ; END="$(date)" ; echo -e "  END=${END}" ; echo -e "START=${START}"

[CMAQ_BC_IC_plus_N2O issues]: ../../issues
[regrid_utils]: https://bitbucket.org/epa_n2o_project_team/regrid_utils
[regrid_utils/repo_diff.sh]: https://bitbucket.org/epa_n2o_project_team/regrid_utils/src/HEAD/repo_diff.sh?at=master
[BC_IC_helpers.ncl]: ../../src/HEAD/BC_IC_helpers.ncl?at=master
[IOAPI_spatiotemporality_checker/IOAPIST_helpers.ncl]: https://bitbucket.org/epa_n2o_project_team/ioapi_spatiotemporality_checker/src/HEAD/IOAPIST_helpers.ncl?at=master
