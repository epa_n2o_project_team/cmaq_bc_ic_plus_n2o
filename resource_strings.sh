### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# constants with some simple manipulations
# ----------------------------------------------------------------------

### details of this project
LOCAL_PROJECT_NAME='CMAQ_BC_IC_plus_N2O' # TODO: get from commandline
## where this is hosted: follow bitbucket conventions
REMOTE_PROJECT_NAME="$(echo ${LOCAL_PROJECT_NAME} | tr '[:upper:]' '[:lower:]')" # bash >= 4: "${LOCAL_PROJECT_NAME,,}"
REMOTE_PROJECT_GIT_URI="git@bitbucket.org:tlroche/${REMOTE_PROJECT_NAME}.git"
REMOTE_PROJECT_HTTP_URI="https://bitbucket.org/epa_n2o_project_team/${REMOTE_PROJECT_NAME}"

### details of this project's code dependency
# path to a project, not a .git
REGRID_UTILS_URI='https://bitbucket.org/epa_n2o_project_team/regrid_utils'
REGRID_UTILS_PN="$(basename ${REGRID_UTILS_URI})" # project name
# can also use 'git@bitbucket.org:tlroche/regrid_utils' if supported

### needed resources (see setup_resources)
export PROJECT_DIR="${HOME}/code/regridding/${LOCAL_PROJECT_NAME}"
export WORK_DIR="${PROJECT_DIR}"
# export RESOURCE_ROOT='..' # nope! relative to ${WORK_DIR}

### model/inventory constants
## canonical extensions
export NETCDF_EXT_NCL='nc'   # NCL prefers the "canonical" netCDF extension
export NETCDF_EXT_CMAQ='ncf' # CMAQ, as is so often the case, is nonstandard :-(
## one template to bind them all ...
export TEMPLATE_STRING='@@@@@@@@' # YYYYMMDD, also accommodates YYMMDD
export MONTHS_PER_YEAR='12'
export HOURS_PER_DAY='24'
export PROJECT_HISTORY="see ${REMOTE_PROJECT_URI}"

# IOAPI metadata: see http://www.baronams.com/products/ioapi/INCLUDE.html#fdesc
export IOAPI_TIMESTEP_LENGTH='10000' # :TSTEP
export IOAPI_START_OF_TIMESTEPS='0'  # :STIME
export IOAPI_VARLIST_NAME='VAR-LIST' # attributes with dashes make NCL vomit
export IOAPI_TFLAG_NAME='TFLAG'
export IOAPI_VERT_SCHEMA_ATTR_NAME='VGTYP'
export IOAPI_VERT_SCHEMA_INT=7 # i.e., we only currently support VGTYP=7 -> VGWRFEM
# For semantics of VGTYP=7/VGWRFEM, see m3user thread @ http://lists.unc.edu/read/messages?id=7014822
export IOAPI_TOP_LEVEL_ATTR_NAME='VGTOP' # name of global attribute quantifying top level (in Pascals for VGTYP=7/VGWRFEM per Carlie Coats @ http://lists.unc.edu/read/archive?id=7015202 )
export IOAPI_VERT_LEVELS_ATTR_NAME='VGLVLS'
# TODO: also check that number of layers match!

### inputs

## NH N2O

# file
WIDE_AREA_N2O_FN='N2O_conc_NH_monthly.nc'
export WIDE_AREA_N2O_FP="${WORK_DIR}/${WIDE_AREA_N2O_FN}"
# attributes
export WIDE_AREA_N2O_FILE_ATTR_TITLE='calculated northern hemisphere mean N2O concentrations, monthly'
export WIDE_AREA_N2O_FILE_ATTR_SOURCE_FILE='ftp://ftp.cmdl.noaa.gov/hats/n2o/combined/HATS_global_N2O.txt'
export WIDE_AREA_N2O_FILE_ATTR_CONVENTIONS='None'

# dimensions
export WIDE_AREA_N2O_DIM_MONTH_NAME='month'
export WIDE_AREA_N2O_DIM_YEAR_NAME='year'
# attributes
export WIDE_AREA_N2O_DIM_MONTH_ATTR_UNITS='integer month in year'
export WIDE_AREA_N2O_DIM_YEAR_ATTR_UNITS='integer year in Common Era'

# datavar
export WIDE_AREA_N2O_VAR_NAME='N2O'
export WIDE_AREA_N2O_VAR_TYPE='float'
# attributes
export WIDE_AREA_N2O_VAR_ATTR_UNITS='ppm N2O'
export WIDE_AREA_N2O_VAR_ATTR_LONGNAME="$(printf '%-16s' ${WIDE_AREA_N2O_VAR_NAME})"
export WIDE_AREA_N2O_VAR_ATTR_VARDESC="$(printf '%-80s' "Variable ${WIDE_AREA_N2O_VAR_NAME} derived from NOAA ESRL HATS northern-hemisphere monthly mean conc")"

## {BC, IC} dirs contain {raw, unverticalized, AQ-only} files only

## boundary conditions (BCs)
#export BC_AQ_RAW_DIR='/work/MOD3APP/rtd/BC/2008cdc/AQ_raw'      # on terrae
export BC_AQ_RAW_DIR='/project/inf35w/roche/BC/2008cdc/AQ_raw'  # on infinity

# Optionally, user can provide a list of BC filepaths to process:
# a file with one filepath per line, to avoid (all but the most egregious) length constraints:
BCs_TO_PROCESS_DIR="${WORK_DIR}"
BCs_TO_PROCESS_FN='BCs_to_process.txt'
export BCs_TO_PROCESS_FP="${BCs_TO_PROCESS_DIR}/${BCs_TO_PROCESS_FN}"

export BC_FN_PREFIX='BCON' # ASSERT: all BC filenames begin with this

## initial conditions (ICs)
#export IC_AQ_RAW_DIR='/work/MOD3APP/rtd/IC/2008cdc/AQ_raw'      # on terrae
export IC_AQ_RAW_DIR='/project/inf35w/roche/IC/2008cdc/AQ_raw'  # on infinity
# I don't believe I need to process *all* ICs in ${IC_AQ_RAW_DIR}: most will be created by model run?
# So pass a list of IC filepaths to process, ...

# ... and contain the list in a file (one filepath per line), to avoid (all but the most egregious) length constraints:
ICs_TO_PROCESS_DIR="${WORK_DIR}"
ICs_TO_PROCESS_FN='ICs_to_process.txt'
export ICs_TO_PROCESS_FP="${ICs_TO_PROCESS_DIR}/${ICs_TO_PROCESS_FN}"

export IC_FN_PREFIX='ICON' # ASSERT: all IC filenames begin with this

## meteorology
## 3D met files required for reverticalizing due to OAQPS error (see https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/OAQPS_CDC_PHASE_2008#!vertical-mismatch )
# folder containing 3D meteorology with reference vertical levels
export MET_DIR='/project/inf35w/roche/met/MCIP_v3.6/WRF_2008_24aL/12US1/mcip_out' # export?
# filename template for 3D met files to consult: "6date" ~= YYMMDD
export MET_3D_FN_TEMPLATE="METBDY3D_${TEMPLATE_STRING}"                           # export?
export MET_3D_FP_TEMPLATE="${MET_DIR}/${MET_3D_FN_TEMPLATE}"

### intermediate directories for {verticalized, AQ-only} files

## boundary conditions (BCs)
#export BC_AQ_VERT_DIR='/work/MOD3APP/rtd/BC/2008cdc/AQ_vert'      # on terrae
export BC_AQ_VERT_DIR='/project/inf35w/roche/BC/2008cdc/AQ_vert'  # on infinity

## initial conditions (ICs)
#export IC_AQ_VERT_DIR='/work/MOD3APP/rtd/IC/2008cdc/AQ_vert'      # on terrae
export IC_AQ_VERT_DIR='/project/inf35w/roche/IC/2008cdc/AQ_vert'  # on infinity

### output directories for {verticalized, N2O-augmented} files

### output directories for {verticalized, N2O-augmented} files

# do following to regenerate output:
# export BC_AUG_DIR="$(mktemp -d)"
# export IC_AUG_DIR="$(mktemp -d)"
# otherwise:

## boundary conditions (BCs)
#export BC_AUG_DIR='/work/MOD3APP/rtd/BC/2008cdc/AQ_vert_plus_N2O' # on terrae
export BC_AUG_DIR='/project/inf35w/roche/BC/2008cdc/AQ_vert_plus_N2O' # on infinity

## initial conditions (ICs)
#export IC_AUG_DIR='/work/MOD3APP/rtd/IC/2008cdc/AQ_vert_plus_N2O'     # on terrae
export IC_AUG_DIR='/project/inf35w/roche/IC/2008cdc/AQ_vert_plus_N2O' # on infinity

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

### Shared with driver and uber_driver
function setup_resources {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  for CMD in \
    'setup_sink_augmented_BCs' \
    'setup_sink_augmented_ICs' \
    'setup_sink_verticalized_BCs' \
    'setup_sink_verticalized_ICs' \
  ; do
    echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
      exit 1
    fi
  done
} # end function setup_resources

### Preposition/stage verticalized BCs
function setup_sink_verticalized_BCs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   [[ -z "${BC_AQ_VERT_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} BC_AQ_VERT_DIR undefined"
    exit 2
  elif [[ ! -d "${BC_AQ_VERT_DIR}" ]] ; then
    for CMD in \
      "mkdir -p ${BC_AQ_VERT_DIR}" \
    ; do
      echo -e "${MESSAGE_PREFIX}:${CMD}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        if [[ -n "${ACCESS}" ]] ; then # ACCESS defined under uber_driver
          echo -e "${ERROR_PREFIX} '${CMD}' failed (with ACCESS==${ACCESS}), exiting ..."
        else
          echo -e "${ERROR_PREFIX} '${CMD}' failed, exiting ..."
        fi
        exit 3
      fi
    done
  fi
} # end function setup_sink_verticalized_BCs

### Preposition/stage verticalized ICs
function setup_sink_verticalized_ICs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   [[ -z "${IC_AQ_VERT_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} IC_AQ_VERT_DIR undefined"
    exit 4
  elif [[ ! -d "${IC_AQ_VERT_DIR}" ]] ; then
    for CMD in \
      "mkdir -p ${IC_AQ_VERT_DIR}" \
    ; do
      echo -e "${MESSAGE_PREFIX}:${CMD}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        if [[ -n "${ACCESS}" ]] ; then # ACCESS defined under uber_driver
          echo -e "${ERROR_PREFIX} '${CMD}' failed (with ACCESS==${ACCESS}), exiting ..."
        else
          echo -e "${ERROR_PREFIX} '${CMD}' failed, exiting ..."
        fi
        exit 5
      fi
    done
  fi
} # end function setup_sink_verticalized_ICs

### Preposition/stage augmented BCs
function setup_sink_augmented_BCs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   [[ -z "${BC_AUG_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} BC_AUG_DIR undefined"
    exit 6
  elif [[ ! -d "${BC_AUG_DIR}" ]] ; then
    for CMD in \
      "mkdir -p ${BC_AUG_DIR}" \
    ; do
      echo -e "${MESSAGE_PREFIX}:${CMD}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        if [[ -n "${ACCESS}" ]] ; then # ACCESS defined under uber_driver
          echo -e "${ERROR_PREFIX} '${CMD}' failed (with ACCESS==${ACCESS}), exiting ..."
        else
          echo -e "${ERROR_PREFIX} '${CMD}' failed, exiting ..."
        fi
        exit 7
      fi
    done
  fi
} # end function setup_sink_augmented_BCs

### Preposition/stage augmented ICs
function setup_sink_augmented_ICs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   [[ -z "${IC_AUG_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} IC_AUG_DIR undefined"
    exit 8
  elif [[ ! -d "${IC_AUG_DIR}" ]] ; then
    for CMD in \
      "mkdir -p ${IC_AUG_DIR}" \
    ; do
      echo -e "${MESSAGE_PREFIX}:${CMD}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        if [[ -n "${ACCESS}" ]] ; then # ACCESS defined under uber_driver
          echo -e "${ERROR_PREFIX} '${CMD}' failed (with ACCESS==${ACCESS}), exiting ..."
        else
          echo -e "${ERROR_PREFIX} '${CMD}' failed, exiting ..."
        fi
        exit 9
      fi
    done
  fi
} # end function setup_sink_augmented_ICs
