#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### A top-level driver for integrating species=N2O into existing CMAQ AQ boundary-condition (BC) and initial-condition (IC) files over the same horizontal domain.

### See https://bitbucket.org/epa_n2o_project_team/cmaq_bc_ic_plus_n2o

### Expects to run on linux. Utility dependencies include:

### * definitely

### ** bash: required to run this script. Known to work with bash --version==3.2.25

### * potentially initially. If you have not already retrieved regrid_utils (see `get_regrid_utils`), the following must be available to the script when initially run:

### ** git: to clone their repo
### ** web access (HTTP currently)

### * ultimately

### ** basename, dirname
### ** cp
### ** ncdump
### ** NCL

###   Paths to the above should be setup in `bash_utilities::setup_paths`

### Note: if ${THIS} not run from uber_driver.sh, you may need to set ${GIT_CLONE_PREFIX} on some platforms (e.g., EMVL/terrae). 
### See uber_driver.sh for details.

### TODO: failing functions should fail this (entire) driver!

### Configure as needed for your platform.

# ----------------------------------------------------------------------
# constants with some simple manipulations
# ----------------------------------------------------------------------

# TODO: take switches for help, debugging, no/eval, target drive
THIS="$0"
THIS_FN="$(basename ${THIS})"
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### workspace
# note: following will fail if THIS_FN is `source`d!
# source "${WORK_DIR}/resource_strings.sh" # fails, since WORK_DIR is defined in itself!
source "${THIS_DIR}/resource_strings.sh" # allow reuse in uber_driver.sh

# ----------------------------------------------------------------------
# helpers
# ----------------------------------------------------------------------

### helpers in this repo

export BC_IC_FUNCS_FN='BC_IC_helpers.ncl' # not a regrid_utils
export BC_IC_FUNCS_FP="${WORK_DIR}/${BC_IC_FUNCS_FN}"

export VERTICALIZER_FN='verticalize_conditions.ncl'
export VERTICALIZER_FP="${WORK_DIR}/${VERTICALIZER_FN}"

export WRITE_N2O_FN='write_N2O.ncl'
export WRITE_N2O_FP="${WORK_DIR}/${WRITE_N2O_FN}"

export AUGMENTER_FN='conditions_plus_N2O.ncl'
export AUGMENTER_FP="${WORK_DIR}/${AUGMENTER_FN}"

# export BC_AUG_FN='BC_plus_N2O.ncl'
# export BC_AUG_FP="${WORK_DIR}/${BC_AUG_FN}"

# export IC_AUG_FN='IC_plus_N2O.ncl'
# export IC_AUG_FP="${WORK_DIR}/${IC_AUG_FN}"

### helpers retrieved from elsewhere. TODO: create NCL and R packages

# path to a folder, not a file: needed by NCL to get to initial helpers
export REGRID_UTILS_DIR="${WORK_DIR}/${REGRID_UTILS_PN}" # folder, not file

## TODO: move these back to regrid_utils
## But allow user to override repo code: see (e.g.) `get_bash_utils`

export ATTR_FUNCS_FN='attributes.ncl'
export ATTR_FUNCS_FP="${WORK_DIR}/${ATTR_FUNCS_FN}"

export BASH_UTILS_FN='bash_utilities.sh'
export BASH_UTILS_FP="${WORK_DIR}/${BASH_UTILS_FN}"

export COMPARE_FUNCS_FN='compare.ncl'
export COMPARE_FUNCS_FP="${WORK_DIR}/${COMPARE_FUNCS_FN}"

export DIM_FUNCS_FN='dimensions.ncl'
export DIM_FUNCS_FP="${WORK_DIR}/${DIM_FUNCS_FN}"

export FILEPATH_FUNCS_FN='get_filepath_from_template.ncl'
export FILEPATH_FUNCS_FP="${WORK_DIR}/${FILEPATH_FUNCS_FN}"

export IOAPI_FUNCS_FN='IOAPI.ncl'
export IOAPI_FUNCS_FP="${WORK_DIR}/${IOAPI_FUNCS_FN}"

export STRING_FUNCS_FN='string.ncl'
export STRING_FUNCS_FP="${WORK_DIR}/${STRING_FUNCS_FN}"

# export SUMMARIZE_FUNCS_FN='summarize.ncl'
# export SUMMARIZE_FUNCS_FP="${WORK_DIR}/${SUMMARIZE_FUNCS_FN}"

export TIME_FUNCS_FN='time.ncl'
export TIME_FUNCS_FP="${WORK_DIR}/${TIME_FUNCS_FN}"

### For strings relating to raw inputs and intermediate and final outputs, see resource_strings.sh

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# setup functions
# ----------------------------------------------------------------------

### TODO: test for resources, reuse if available
### `setup_paths` isa bash util, so `get_helpers` first
### `setup_apps` ditto

#     "mkdir -p ${WORK_DIR}" \
#     'get_helpers' \
#     'get_input_filepath_lists' \
#     'setup_paths' \
#     'setup_apps' \
#     'setup_resources' \
function setup {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local LOOP_MESSAGE_PREFIX
  local LOOP_ERROR_PREFIX

  for CMD in \
    "mkdir -p ${WORK_DIR}" \
    'get_helpers' \
    'get_input_filepath_lists' \
    'setup_paths' \
    'setup_apps' \
    'setup_resources' \
  ; do
    LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
    LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
    echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
      exit 2
    fi
  done
#  echo -e "\n$ ${THIS_FN}: PDF_VIEWER='${PDF_VIEWER}'" # debugging
} # end function setup

### Get needed helper modules. Note 'get_regrid_utils' must go first, since latter rely on it.
#    'get_check_conservation' \
function get_helpers {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local LOOP_MESSAGE_PREFIX
  local LOOP_ERROR_PREFIX

#     'get_regrid_utils' \
#     'get_bash_utils' \
#     'get_BC_IC_funcs' \
#     'get_IOAPI_funcs' \
#     'get_string_funcs' \
#     'get_time_funcs' \
#     'get_verticalizer' \
#     'get_write_N2O' \
#     'get_augmenter' \
  for CMD in \
    'get_regrid_utils' \
    'get_bash_utils' \
    'get_BC_IC_funcs' \
    'get_IOAPI_funcs' \
    'get_string_funcs' \
    'get_time_funcs' \
    'get_verticalizer' \
    'get_write_N2O' \
    'get_augmenter' \
  ; do
    LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
    LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
    echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
      exit 3
    fi
  done
} # end function get_helpers

### This gets "package"=regrid_utils from which the remaining helpers are (at least potentially) drawn.
function get_regrid_utils {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local LOOP_MESSAGE_PREFIX
  local LOOP_ERROR_PREFIX

  if [[ -z "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} REGRID_UTILS_DIR not defined"
    exit 4
  fi

  # Noticed a problem where code committed to regrid_utils was not replacing code in existing ${REGRID_UTILS_DIR}, so ...
  if [[ -r "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${MESSAGE_PREFIX} removing existing regrid_utils folder='${REGRID_UTILS_DIR}'"
    for CMD in \
      "rm -fr ${REGRID_UTILS_DIR}/" \
     ; do
      LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
      LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
      echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
        exit 5
      fi
    done
  fi

  if [[ ! -r "${REGRID_UTILS_DIR}" ]] ; then
  # assumes GIT_CLONE_PREFIX set in environment by, e.g., uber_driver
    for CMD in \
      "${GIT_CLONE_PREFIX} git clone ${REGRID_UTILS_URI}.git ${REGRID_UTILS_DIR}/" \
    ; do
      LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
      LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
      echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo # newline
        echo -e "${LOOP_ERROR_PREFIX} failed or not found"
        echo -e "\t(suggestion: check that GIT_CLONE_PREFIX is set appropriately in calling environment)"
        echo # newline
        exit 6
      fi
    done
  fi

  if [[ ! -r "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot download regrid_utils to '${REGRID_UTILS_DIR}'"
    exit 7
  fi  
} # end function get_regrid_utils

### isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
### To override, copy/mod to ${BASH_UTILS_FP} before running this script.
function get_bash_utils {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local LOOP_MESSAGE_PREFIX
  local LOOP_ERROR_PREFIX

  if [[ -z "${BASH_UTILS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} BASH_UTILS_FP not defined"
    exit 8
  fi
  if [[ ! -r "${BASH_UTILS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${BASH_UTILS_FN} ${BASH_UTILS_FP}" \
    ; do
      LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
      LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
      echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
        exit 9
      fi
    done
  fi
  if [[ ! -r "${BASH_UTILS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} BASH_UTILS_FP=='${BASH_UTILS_FP}' not readable"
    exit 10
  fi
  # This is bash, so gotta ...
  source "${BASH_UTILS_FP}"
  # ... for its functions to be available later in this script
} # end function get_bash_utils

### !isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
### To override, copy/mod to ${BC_IC_FUNCS_FP} before running this script.
function get_BC_IC_funcs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${BC_IC_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} BC_IC_FUNCS_FP not defined"
    exit 11
  fi
  # is in this repo, not (yet?) a regrid_utils
#   if [[ ! -r "${BC_IC_FUNCS_FP}" ]] ; then
#     # copy from downloaded regrid_utils
#     for CMD in \
#       "cp ${REGRID_UTILS_DIR}/${BC_IC_FUNCS_FN} ${BC_IC_FUNCS_FP}" \
#     ; do
#       echo -e "$ ${CMD}"
#       eval "${CMD}"
#     done
#   fi
  if [[ ! -r "${BC_IC_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} BC_IC_FUNCS_FP=='${BC_IC_FUNCS_FP}' not readable"
    exit 12
  fi
} # end function get_BC_IC_funcs

### isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
### To override, copy/mod to ${IOAPI_FUNCS_FP} before running this script.
function get_IOAPI_funcs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${IOAPI_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} IOAPI_FUNCS_FP not defined"
    exit 13
  fi
  if [[ ! -r "${IOAPI_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${IOAPI_FUNCS_FN} ${IOAPI_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${IOAPI_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} IOAPI_FUNCS_FP=='${IOAPI_FUNCS_FP}' not readable"
    exit 14
  fi
} # end function get_IOAPI_funcs

### isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
### To override, copy/mod to ${STRING_FUNCS_FP} before running this script.
function get_string_funcs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${STRING_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} STRING_FUNCS_FP not defined"
    exit 15
  fi
  if [[ ! -r "${STRING_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${STRING_FUNCS_FN} ${STRING_FUNCS_FP}" \
    ; do
      echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
        exit 16
      fi
    done
  fi
  if [[ ! -r "${STRING_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} STRING_FUNCS_FP=='${STRING_FUNCS_FP}' not readable"
    exit 17
  fi
} # end function get_string_funcs

### isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
### To override, copy/mod to ${TIME_FUNCS_FP} before running this script.
function get_time_funcs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${TIME_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} TIME_FUNCS_FP not defined"
    exit 18
  fi
  if [[ ! -r "${TIME_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${TIME_FUNCS_FN} ${TIME_FUNCS_FP}" \
    ; do
      echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
        exit 19
      fi
    done
  fi
  if [[ ! -r "${TIME_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} TIME_FUNCS_FP=='${TIME_FUNCS_FP}' not readable"
    exit 20
  fi
} # end function get_time_funcs

function get_verticalizer {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${VERTICALIZER_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} VERTICALIZER_FP not defined"
    exit 21
  fi
  # is in this repo
#  if [[ ! -r "${VERTICALIZER_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${VERTICALIZER_FP} ${VERTICALIZER_URI}" \
#    ; do
#      echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${VERTICALIZER_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} VERTICALIZER_FP=='${VERTICALIZER_FP}' not readable"
    exit 22
  fi
} # end function get_verticalizer

### Get lists of filepaths for the raw/unverticalized input *C files.
function get_input_filepath_lists {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local LOOP_MESSAGE_PREFIX
  local LOOP_ERROR_PREFIX
#  local TYPE_TAG # in `for` loop, therefore local
  local INPUT_DIR
  local INPUT_LIST_FP
  local INPUT_FN_PREFIX

  for TYPE_TAG in 'BC' 'IC' ; do
    if   [[ "${TYPE_TAG}" == 'BC' ]] ; then
      INPUT_LIST_FP="${BCs_TO_PROCESS_FP}"
      INPUT_DIR="${BC_AQ_RAW_DIR}"
      INPUT_FN_PREFIX="${BC_FN_PREFIX}*"
    elif [[ "${TYPE_TAG}" == 'IC' ]] ; then
      INPUT_LIST_FP="${ICs_TO_PROCESS_FP}"
      INPUT_DIR="${IC_AQ_RAW_DIR}"
      INPUT_FN_PREFIX="${IC_FN_PREFIX}*"
    else
      echo -e "${ERROR_PREFIX} cannot process TYPE_TAG='${TYPE_TAG}', exiting"
      exit 23
    fi

    if [[ -r "${INPUT_LIST_FP}" ]] ; then
      echo -e "${MESSAGE_PREFIX} about to process list of ${TYPE_TAG} inputs in file='${INPUT_LIST_FP}'"
    else ## write the list file
      # TODO: test ${INPUT_DIR} , ${INPUT_FN_PREFIX}
      for CMD in \
        "find ${INPUT_DIR}/ -maxdepth 1 -name '${INPUT_FN_PREFIX}' | sort > ${INPUT_LIST_FP}" \
      ; do
        LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
        LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
        echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
        eval "${CMD}" # comment this out for NOPing, e.g., to `source`
        if [[ $? -ne 0 ]] ; then
          echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
          exit 24
        fi
      done
    fi # [[ -r "${INPUT_LIST_FP}" ]]

    if [[ -r "${INPUT_LIST_FP}" ]] ; then
      for CMD in \
        "ls -al ${INPUT_LIST_FP}" \
        "wc -l ${INPUT_LIST_FP}" \
      ; do
        LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
        LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
        echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
        eval "${CMD}" # comment this out for NOPing, e.g., to `source`
        if [[ $? -ne 0 ]] ; then
          echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
          exit 25
        fi
      done
    else
      echo -e "\n${ERROR_PREFIX} cannot read INPUT_LIST_FP=${INPUT_LIST_FP}\n"
    fi # [[ -r "${INPUT_LIST_FP}" ]]

  done # for TYPE_TAG in 'BC' 'IC'

} # function get_input_filepath_lists

### TODO: document me!
function get_write_N2O {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${WRITE_N2O_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} WRITE_N2O_FP not defined"
    exit 26
  fi
  # is in this repo
#  if [[ ! -r "${WRITE_N2O_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${WRITE_N2O_FP} ${WRITE_N2O_URI}" \
#    ; do
#      echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${WRITE_N2O_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} WRITE_N2O_FP=='${WRITE_N2O_FP}' not readable"
    exit 27
  fi
} # end function get_write_N2O

### TODO: document me!
function get_augmenter {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${AUGMENTER_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} AUGMENTER_FP not defined"
    exit 28
  fi
  # is in this repo
#  if [[ ! -r "${AUGMENTER_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${AUGMENTER_FP} ${AUGMENTER_URI}" \
#    ; do
#      echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${AUGMENTER_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} AUGMENTER_FP=='${AUGMENTER_FP}' not readable"
    exit 29
  fi
} # end function get_augmenter

# ----------------------------------------------------------------------
# action functions
# ----------------------------------------------------------------------

### Ensure *Cs ("conditions files") are vertically consistent with our 3D meteorology.
### For motivation, see https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/OAQPS_CDC_PHASE_2008#!vertical-mismatch
### Depends on function get_verticalizer above (was get_BC_verticalizer, get_IC_verticalizer)
### Assumes `ncl` is in PATH: see bash_utilities.sh
### TODO:
### * refactor with the very-similar function=verticalize_conditions
function verticalize_conditions {
  local TYPE_TAG="${1}"
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local LOOP_MESSAGE_PREFIX
  local LOOP_ERROR_PREFIX
  local VERDI_SAMPLE_SPECIES='NO2'  # TODO: move to resource_strings
  local VERDI_SPECIES_ARG="${VERDI_SAMPLE_SPECIES}[1]"

  if   [[ -z "${TYPE_TAG}" ]] ; then
    echo -e "${ERROR_PREFIX} do not know which type of conditions to process, exiting ..."
    exit 32
  elif [[ "${TYPE_TAG}" == 'BC' ]] ; then
    local FILES_TO_PROCESS_LIST_FP="${BCs_TO_PROCESS_FP}"
    local OUTPUT_DIR="${BC_AQ_VERT_DIR}"
  elif [[ "${TYPE_TAG}" == 'IC' ]] ; then
    local FILES_TO_PROCESS_LIST_FP="${ICs_TO_PROCESS_FP}"
    local OUTPUT_DIR="${IC_AQ_VERT_DIR}"
  else
    echo -e "${ERROR_PREFIX} do not know how to process conditions type='${TYPE_TAG}', exiting ..."
    exit 33
  fi # [[ -z "${TYPE_TAG}" ]]

  if   [[ ! -r "${FILES_TO_PROCESS_LIST_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read file with list of files to process='${FILES_TO_PROCESS_LIST_FP}', exiting"
    exit 34
  elif [[ ! -r "${VERTICALIZER_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read *C processor script='${VERTICALIZER_FP}', exiting"
    exit 35
  else # process the list of files!
#    ncl # bail to NCL and copy script lines, or ...
  # not-so-cleverly avoiding an `export`
    for CMD in \
      "TYPE_TAG='${TYPE_TAG}' ncl -n ${VERTICALIZER_FP}" \
    ; do
      LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
      LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
      cat <<EOM

About to run command="${CMD}"

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
        exit 36
      fi
    done

    ## Check output:
    ## * list all output
    for CMD in \
      "ls -al ${OUTPUT_DIR}" \
    ; do
      LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
      LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
      echo -e "${LOOP_MESSAGE_PREFIX}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
        exit 37
      fi
    done

    ## * `ncdump` last output
    LAST_FP="$(find "${OUTPUT_DIR}" -type f | sort | tail -n 1)"
    if   [[ -z "${LAST_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} failed to find LAST_FP, exiting"
      exit 38
    elif [[ ! -r "${LAST_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} failed to find last IC='${LAST_FP}', exiting"
      exit 39
    else
      for CMD in \
        "ncdump -h ${LAST_FP}" \
      ; do
        LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
        LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
        echo -e "${LOOP_MESSAGE_PREFIX}"
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
          echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
          exit 40
        fi
      done
    fi # [[ -z "${LAST_FP}" ]]

    ## * iff IC (VERDI don't do BCs): VERDI last output to verify its IOAPI-well-formedness
    if [[ "${TYPE_TAG}" == 'IC' ]] ; then
      for CMD in \
        "verdi -f '${LAST_FP}' -s '${VERDI_SPECIES_ARG}' -gtype tile &" \
      ; do
        LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
        LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
        echo -e "${LOOP_MESSAGE_PREFIX}"
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
          echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
          exit 41
        fi
      done
    fi # [[ -z "${TYPE_TAG}" ]]
  fi # [[ ! -r "${FILES_TO_PROCESS_LIST_FP}" ]]

} # end function verticalize_conditions

### Get source for N2O data: write if not already readable.
### Depends on function get_write_N2O above.
### Assumes `ncl` is in PATH: see bash_utilities.sh
function get_N2O {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local LOOP_MESSAGE_PREFIX
  local LOOP_ERROR_PREFIX

#   ncl # bail to NCL and copy script lines, or ...
  if   [[ -z "${WIDE_AREA_N2O_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} WIDE_AREA_N2O_FP undefined"
    exit 42
  elif [[ -r "${WIDE_AREA_N2O_FP}" ]] ; then
    echo -e "${MESSAGE_PREFIX}: found N2O datasource='${WIDE_AREA_N2O_FP}', will not recreate"
  else
    for CMD in \
      "ncl -n ${WRITE_N2O_FP}" \
    ; do
      LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
      LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
      cat <<EOM

About to run command="${CMD}"

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
        exit 43
      fi
    done
  fi # [[ -r "${WIDE_AREA_N2O_FP}" ]] # take 1

  ## What do we have?
  for CMD in \
    "ls -al ${WIDE_AREA_N2O_FP}" \
    "ncdump -h ${WIDE_AREA_N2O_FP}" \
  ; do
    LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
    LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
    echo -e "${LOOP_MESSAGE_PREFIX}"
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
      exit 41
    fi
  done

} # end function get_N2O

### Ensure *Cs ("conditions files") are vertically consistent with our 3D meteorology.
### For motivation, see https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/OAQPS_CDC_PHASE_2008#!vertical-mismatch
### Depends on function get_verticalizer above (was get_BC_verticalizer, get_IC_verticalizer)
### Assumes `ncl` is in PATH: see bash_utilities.sh
### TODO:
### * refactor with the very-similar function=verticalize_conditions
function augment_conditions {
  local TYPE_TAG="${1}"
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local LOOP_MESSAGE_PREFIX
  local LOOP_ERROR_PREFIX
  local VERDI_SAMPLE_SPECIES='N2O'  # TODO: move to resource_strings
  local VERDI_SPECIES_ARG="${VERDI_SAMPLE_SPECIES}[1]"

  if   [[ -z "${TYPE_TAG}" ]] ; then
    echo -e "${ERROR_PREFIX} do not know which type of conditions to process, exiting ..."
    exit 44
  elif [[ "${TYPE_TAG}" == 'BC' ]] ; then
    local OUTPUT_DIR="${BC_AUG_DIR}"
  elif [[ "${TYPE_TAG}" == 'IC' ]] ; then
    local OUTPUT_DIR="${IC_AUG_DIR}"
  else
    echo -e "${ERROR_PREFIX} do not know how to process conditions type='${TYPE_TAG}', exiting ..."
    exit 45
  fi # [[ -z "${TYPE_TAG}" ]]

  if   [[ ! -r "${AUGMENTER_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read augmenter script='${AUGMENTER_FP}', exiting"
    exit 46
  else
#    ncl # bail to NCL and copy script lines, or ...
  # not-so-cleverly avoiding an `export`
    for CMD in \
      "TYPE_TAG='${TYPE_TAG}' ncl -n ${AUGMENTER_FP}" \
    ; do
      LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
      LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
      cat <<EOM

About to run command="${CMD}"

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
        exit 47
      fi
    done

    ## Check output:
    ## * list all output
    for CMD in \
      "ls -al ${OUTPUT_DIR}" \
    ; do
      LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
      LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
      echo -e "${LOOP_MESSAGE_PREFIX}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
        exit 48
      fi
    done

    ## * TODO: check sizeof(output) vs sizeof(input)

    ## * `ncdump` last output
    LAST_FP="$(find "${OUTPUT_DIR}" -type f | sort | tail -n 1)"
    if   [[ -z "${LAST_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} failed to find LAST_FP, exiting"
      exit 49
    elif [[ ! -r "${LAST_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} failed to find last IC='${LAST_FP}', exiting"
      exit 50
    else
      for CMD in \
        "ncdump -h ${LAST_FP}" \
      ; do
        LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
        LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
        echo -e "${LOOP_MESSAGE_PREFIX}"
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
          echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
          exit 51
        fi
      done
    fi # [[ -z "${LAST_FP}" ]]

    ## * iff IC (VERDI don't do BCs): VERDI last output to verify its IOAPI-well-formedness
    if [[ "${TYPE_TAG}" == 'IC' ]] ; then
      for CMD in \
        "verdi -f '${LAST_FP}' -s '${VERDI_SPECIES_ARG}' -gtype tile &" \
      ; do
        LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
        LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
        echo -e "${LOOP_MESSAGE_PREFIX}"
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
          echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
          exit 52
        fi
      done
    fi # [[ -z "${TYPE_TAG}" ]]
  fi # [[ ! -r "${FILES_TO_PROCESS_LIST_FP}" ]]

} # end function augment_conditions

### Do whatever is necessary at the end of a run.
function teardown {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  echo -e "${MESSAGE_PREFIX} nothing to do"
} # end function teardown

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

#   'setup' \            # paths, helpers, etc
#   "verticalize_conditions 'BC'" \ # ensure our BCs are vertically consistent with our meteorology
#   "verticalize_conditions 'IC'" \ # ensure our ICs are vertically consistent with our meteorology
#   'get_N2O' \                     # access/create N2O data suitable for ...
#   "augment_conditions 'BC'" \     # ... "injecting" species=N2O into our BCs ...
#   "augment_conditions 'IC'" \     # ... and our ICs
#   'teardown' \
# should always
# * begin with `setup` to do `module add`
# * end with `teardown` for tidy and testing (e.g., plot display)
for CMD in \
  'setup' \
  "verticalize_conditions 'BC'" \
  "verticalize_conditions 'IC'" \
  'get_N2O' \
  "augment_conditions 'BC'" \
  "augment_conditions 'IC'" \
  'teardown' \
; do
  LOOP_MESSAGE_PREFIX="${THIS_FN}::main loop::${CMD}:"
  LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
  echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
  eval "${CMD}" # comment this out for NOPing, e.g., to `source`
  if [[ $? -ne 0 ]] ; then
    echo -e "\n${LOOP_ERROR_PREFIX} failed or not found\n"
    exit 53
  fi
done

# ----------------------------------------------------------------------
# debugging
# ----------------------------------------------------------------------
